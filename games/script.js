class playGames {
  constructor(pilihan) {
    this.pilihan = pilihan;
  }

  play(player) {
    const computerOptions = ["batu", "kertas", "gunting"];
    const choiceNumber = Math.floor(Math.random() * 3);
    const computer = computerOptions[choiceNumber];
    var result = "";
    if (player === computer) {
      result = "DRAW";
    } else if (player == "batu") {
      if (computer == "gunting") {
        result = "PLAYER <br> WIN";
      } else {
        result = "COM <br> WIN";
      }
    } else if (player == "kertas") {
      if (computer == "batu") {
        result = "PLAYER <br> WIN";
      } else {
        result = "COM <br> WIN";
      }
    } else if (player == "gunting") {
      if (computer == "kertas") {
        result = "PLAYER <br> WIN";
      } else {
        result = "COM <br> WIN";
      }
    }
    const selector_player = document.querySelector("." + player);
    const selectorAi = document.querySelector("." + computer + "Com");
    const selectorMiddle = document.getElementsByClassName("result");
    selector_player.style.backgroundColor = "grey";
    selectorAi.style.backgroundColor = "grey";

    let boxHtml =
      '<div class="box1"><h1 class="resultTxt">' + result + "</h1></div>";
    selectorMiddle[0].innerHTML = boxHtml;
    console.log(result);
  }

  clearBg() {
    const button = document.querySelectorAll(".btn");
    const vsHtml = '<h1 class="middle_text">VS</h1>';
    const selectorMiddle = document.getElementsByClassName("result");
    selectorMiddle[0].innerHTML = vsHtml;
    for (var i = 0; i < button.length; i++) {
      button[i].style.backgroundColor = "rgb(160, 140, 101)";
    }
  }
}

const playGame = (jenis) => {
  const mulaiMain = new playGames();
  mulaiMain.clearBg();
  mulaiMain.play(jenis);
};

const reset = () => {
  const render = new playGames();
  render.clearBg();
};
